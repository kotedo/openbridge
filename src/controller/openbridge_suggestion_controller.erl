-module(openbridge_suggestion_controller, [Req, SessionID]).
-compile(export_all).

%% -------------------------------------------------------------------------------------------------------------------------
%% Created: 06/06/2013 15:12:03 EDT
%% Author : Kai Janson (kai.janson@rackspace.com)
%% Purpose: List the "bridge" items as suggestions
%%          They are fed and controlled by the API
%%          controller. 
%% -------------------------------------------------------------------------------------------------------------------------
index('GET', []) ->
    Bridge = boss_db:find(bridge, []),
    {ok, [{bridge, Bridge}]}.


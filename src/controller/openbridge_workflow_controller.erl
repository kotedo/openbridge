-module(openbridge_workflow_controller, [Req, SessionID]).
-compile(export_all).

index('GET', []) ->
    Workflows = boss_db:find(workflow, [], [{order_by, name},{descending, false}]),
    {ok, [{workflows, Workflows}]}.

create('GET', []) ->
    WorkflowEntities = boss_db:find(workflow_entity, [], [{order_by, name}]),
    {ok, [{workflow_entities, WorkflowEntities}]};
create('POST', []) ->
    %% Getting the needed fields from the form
    Name = Req:post_param("name"),
    Entities = Req:post_param("entities_array"),
    Now = erlang:now(),    
    error_logger:info_msg("Req:post_params():~n~p~n", [Req:post_params()]),

    %% workflow, [Id, Name, CreatedOn, UpdatedOn]).
    %% [Id, Name, BridgeId, CreatedOn, UpdatedOn]).
    case (workflow:new(id, Name, [], Now, Now)):save() of
	{ok, SavedWorkflow} ->
	    %% Attaching the needed Workflow Departments to the new Workflow
	    lists:map( fun(WorkflowId) ->
			       %% [Id, WorkflowId, Name, Email, Weight, CreatedOn, UpdatedOn]).
			       %% Yes, I am duplicating these things, since in the future they might be
			       %% renamed or deleted and I'd like to keep my database sane
			       %% Finding the original one
			       Entity = boss_db:find(WorkflowId),
			       %% and clone it, and save it
			       (workflow_department:new(id, SavedWorkflow:id(), Entity:name(), Entity:email(), Entity:weight(), Now, Now)):save()
		       end, string:tokens(Entities, ",")),
	    {redirect, [{action, "index"}]};
	{error, Reason} ->
	    {redirect, [{action, "create"}, {errors, Reason}]}
    end.

delete('DELETE', ["workflow-" ++ _ = Id] ) ->
    error_logger:info_msg("I would delete id ~p.~n", [Id]),
    case boss_db:delete(Id) of
	ok ->
	    {json, [{status, lists:flatten(io_lib:format("~p deleted.", [Id]))}]};
        Error ->
	    {json, [{error, lists:flatten(io_lib:format("~p occurred deleting Id ~p",[Error, Id]))}]}
    end.

modify('PUT', [Id]) ->
    Old = boss_db:find(Id),
    NewName = Req:post_param("newname"),
    case (Old:set([{name, NewName}])):save() of
	{ok, SavedData} ->
	    {json, [{status, "ok"}]};
	{error, Error} ->
	    {json, [{status, "error"},{error, Error}]}
    end.

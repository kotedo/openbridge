-module(openbridge_workflow_entity_controller, [Req, SessionID]).
-compile(export_all).

%% -----------------------------------------
%% Created: 06/03/2013 14:56:16 EDT
%% Author : Kai Janson (kai.janson@rackspace.com)
%% Purpose: Listing of all Workflow entities
%% -----------------------------------------
index('GET', []) ->
    Workflow_Entities = boss_db:find(workflow_entity, [], [{order_by, name},{order_by,weight}]),
    {ok, [{workflow_entities, Workflow_Entities}]}.

create('GET', []) ->
    ok;
create('POST', []) ->
    %% Getting the needed fields from the form
    Name = Req:post_param("name"),
    Email = Req:post_param("email"),
    Weight = Req:post_param("weight"),
    Now = erlang:now(),
    
    %% workflow_entity, [Id, WorkflowId, Name, Email, Weight, CreatedOn, UpdatedOn])
    case (workflow_entity:new(id, [], Name, Email, Weight, Now, Now)):save() of
	{ok, SavedWorkflowEntity} ->
	    {redirect, [{action, "index"}]};
	{error, Error} ->
	    {redirect, [{action, "create"}, {errors, Error}]}
    end.

delete('DELETE', ["workflow_entity-" ++ _ = Id]) ->
    case boss_db:delete(Id) of
	ok ->
	    {json, [{status, "success"}]};
	{error, Error} ->
	    {json, [{status, "error"},{error, Error}]}
    end.

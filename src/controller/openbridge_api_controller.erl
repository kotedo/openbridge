-module(openbridge_api_controller, [Req, SessionID]).
-compile(export_all).

view('GET', [Id]) ->
    {json, [{status, "no_implemented_yet"}]}.

create('POST', []) ->
    Source = Req:post_param("source"),
    Destination = Req:post_param("destination"),
    Body = Req:post_param("body"),

    error_logger:info_msg("Source     : ~p~n"
			  "Destination: ~p~n"
			  "Body       : ~p~n",[Source, Destination, Body]),
    {json, [{status, "accepted"}]}.

%% update('GET', [Id]) -> ok;
update('PUT', [Id]) ->
    Body = Req:post_params(),
    error_logger:info_msg("PUT~n~nBody: ~p~nID: ~p~n", [Body, Id]),
    {json, [{status, "under_development"}]}.

articles('GET', []) ->
    Bridges = boss_db:find(bridge, []),
    {json, [{bridges, Bridges}]}.

delete('DELETE', [Source, Id, Destination]) ->
    Status = case boss_db:find_first(bridge, [{source, Source}, {id, Id}, {destination, Destination}]) of
		 undefined ->
		     [{status, "error"}, {id, Id ++ " not_found"}];
		 Bridge ->
		     [{status, boss_db:delete(Bridge:id())}]
	     end,
    {json, [{delete, Status}]}.


-module(openbridge_help_controller, [Req, SessionID]).
-compile(export_all).

help('GET', ["entity"]) ->
    Help = "The idea is to create 'Entities' first"
	"These guys can then be included in a Workflow"
	"That part is currently missing,"
	"but next on my todo list"
	"If you look at the entities"
	"The 'Name' is the 'Department/SME' (Should rename it)"
	"the email is the way to get in touch with the team/department/person(s)"
	"the weight is part of the decision process and a way to see how hard it is to 'overrule' that entities opinion",
    ok.

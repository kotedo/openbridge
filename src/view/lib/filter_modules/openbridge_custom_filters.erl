-module(openbridge_custom_filters).
-compile(export_all).

% put custom filters in here, e.g.
%
% my_reverse(Value) ->
%     lists:reverse(binary_to_list(Value)).
%
% "foo"|my_reverse   => "oof"
localtime([]) ->
    "";
localtime(undefined) ->
    "";
localtime(TS) ->
    {{Year, Month, Day},{H,M,S}} = calendar:now_to_local_time(TS),
    lists:flatten(io_lib:format("~2.10.0B/~2.10.0B/~4.10.0B ~2.10.0B:~2.10.0B:~2.10.0B", [Month, Day, Year, H, M, S])).

-module(workflow_entity, [Id, WorkflowId, Name, Email, Weight, CreatedOn, UpdatedOn]).
-compile(export_all).

-belongs_to(workflow).
